<html>
	<head>
		<title>Ejemplo Resolver Operaciones</title>
	</head>
	<body>
		<h1>Resuelva las expresiones presentadas</h1>
	
		<?php
			$i = 9;
			$f = 33.5;
			$c = 'X';
			
			
			echo ($i >= 6) && ($c == 'X'), "<br>";
			echo ($i >= 6) || ($c == 12), "<br>";
			echo ($f < 11) && ($i > 100), "<br>";
			echo ($c != 'P') || (($i + $f) <= 10), "<br>";
			echo  $i + $f <= 10 ,"<br>";
			echo  $i >= 6 && $c == 'X', "<br>";
			echo  $c != 'P' || $i + $f <= 10,  "<br>";
			
            /*Resultados
            Expresion ($i >= 6) && ($c == ‘X’)      	Resultado 1
            Expresion($i >= 6) || ($c == 12)			Resultado 1
            Expresion($f < 11) && ($i > 100)			Resultado
            Expresion($c != ‘P’) || (($i + $f) <= 10)	Resultado 1
            Expresion $i + $f <= 10						Resultado
            Expresion $i >= 6 && $c == ‘X’				Resultado 1
            Expresion $c != ‘P’ || $i + $f <= 10		Resultado 1
            */
			
		?>
	</body>
</html>
